﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankProjectileTripleShot : MonoBehaviour {

    public float Speed;
    /// <summary>
    /// Distance the first shot goes before it splits into 3
    /// </summary>
    public float SingleShotDistance;
    public GameObject WeaponProjectile;

    public float LifeDistance;
    public float DegreeSpread;

    private Rigidbody2D _rigidbody2D;
    private Vector3 _origin;
    private bool isSingle;
    //private Animator _anim;
    // Use this for initialization
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _origin = gameObject.transform.position;
        //_anim = GetComponent<Animator>();
        isSingle = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isSingle && Vector3.Distance(_origin, transform.position) > SingleShotDistance)
        {
            SplitInto3();
        }

        if (!isSingle && Vector3.Distance(_origin, transform.position) > LifeDistance)
        {
            Destroy(gameObject);
            return;
        }

        //converting the object euler angle's magnitude from to Radians    
        var angle = (transform.eulerAngles.magnitude + 90f) * Mathf.Deg2Rad;
        var x = (Mathf.Cos(angle) * Speed);
        var y = (Mathf.Sin(angle) * Speed);
        _rigidbody2D.velocity = new Vector2(x, y); // TODO: Fix this in relation to current rotation (from parent)
    }

    private void SplitInto3()
    {
        isSingle = false;

        var left = Instantiate(WeaponProjectile, transform.position, transform.rotation); // Left
        left.transform.Rotate(Vector3.forward, DegreeSpread);

        Instantiate(WeaponProjectile, transform.position, transform.rotation); // Straight

        var right = Instantiate(WeaponProjectile, transform.position, transform.rotation); // Right
        right.transform.Rotate(Vector3.forward, -DegreeSpread);

        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // TODO: Change to explode animation
        // Explode animation will then trigger the destroy
        Destroy(gameObject);
    }
}
