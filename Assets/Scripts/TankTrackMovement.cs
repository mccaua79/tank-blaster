﻿using UnityEngine;

public class TankTrackMovement : MonoBehaviour
{
    public float Acceleration;
    public string TankTrackName;
    private Animator _animator;
	// Use this for initialization
	void Start ()
	{
	    _animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _animator.SetFloat("Acceleration", Acceleration);
	}
}
