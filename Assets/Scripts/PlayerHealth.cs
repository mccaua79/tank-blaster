﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{

    public float Health, MaxHealth;
    private bool _isDead = false;

    // Use this for initialization
    void Start()
    {
        MaxHealth = Health;
    }

    // Update is called once per frame
    void Update()
    {
        if (Health <= 0)
        {
            _isDead = true;
        }

        if (_isDead)
        {
            Health = 0;
            // TODO: Add animation to explode tank, then after animation is done, reload level
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void TakeDamage(float damage)
    {
        Health -= damage;
    }

    public bool IsDead()
    {
        return _isDead;
    }
}
