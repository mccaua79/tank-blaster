﻿using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private GameObject _weaponProjectile;
    public GameObject ProjectilePosition;
    public GameObject[] Weapons;
    public int WeaponSelected;

    // Use this for initialization
    void Start()
    {
        _weaponProjectile = Weapons[WeaponSelected];
    }

    void Update()
    {
        ChooseWeaponCheck();
    }

    private void ChooseWeaponCheck()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            WeaponSelected = 0;
            _weaponProjectile = Weapons[WeaponSelected];
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            WeaponSelected = 1;
            _weaponProjectile = Weapons[WeaponSelected];
        }
    }

    // Update is called once per frame
    public void FireWeapon()
    {
        Instantiate(_weaponProjectile, ProjectilePosition.transform.position, ProjectilePosition.transform.rotation);
    }
}
