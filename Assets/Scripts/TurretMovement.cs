﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TurretMovement : MonoBehaviour
{
    public float TurretRotationSpeed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate()
	{
	    var joystickConnected = false;
	    var joysticks = Input.GetJoystickNames(); // TODO: I unplugged, it still detects one?
	    var joyCount = joysticks.Length;
	    for (var i = 0; i < joyCount; i++)
	    {
	        var joystick = joysticks[i];
	        if (!string.IsNullOrEmpty(joystick))
	        {
	            joystickConnected = true;
	        }
	    }

        if (joystickConnected)
	    {
	        var rotation = Input.GetAxisRaw("TurretRotation");
	        transform.RotateAround(transform.position, transform.forward, Time.deltaTime * -rotation * TurretRotationSpeed);
	    }
	    else
	    {
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var rot = Quaternion.LookRotation(Vector3.forward, mousePos - transform.position);
	        transform.rotation = Quaternion.Slerp(transform.rotation, rot, TurretRotationSpeed);
	    }
    }
}
