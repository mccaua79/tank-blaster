﻿using UnityEngine;

public class EnemyHealthController : MonoBehaviour {

    public float HitPoints, MaxHitPoints;
    private bool _isDead = false;

    // Use this for initialization
    void Start ()
    {
        MaxHitPoints = HitPoints;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (HitPoints <= 0)
        {
            _isDead = true;
        }

    }

    public void TakeDamage(float damage)
    {
        HitPoints -= damage;
    }

    public bool IsDead()
    {
        return _isDead;
    }
}
