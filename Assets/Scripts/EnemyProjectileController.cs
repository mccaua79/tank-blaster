﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileController : MonoBehaviour {

    public float Speed;
    public float LifeDistance;
    public float ProjectileDamageStrength;

    private Rigidbody2D _rigidbody2D;
    private Vector3 _origin;
    //private Animator _anim;
    // Use this for initialization
    void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _origin = gameObject.transform.position;
        //_anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(_origin, transform.position) > LifeDistance)
        {
            Destroy(gameObject);
            return;
        }

        //converting the object euler angle's magnitude from to Radians    
        var angle = (transform.eulerAngles.magnitude + 270f) * Mathf.Deg2Rad;
        var x = (Mathf.Cos(angle) * Speed);
        var y = (Mathf.Sin(angle) * Speed);
        _rigidbody2D.velocity = new Vector2(x, y); 
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var playerHealth = other.gameObject.GetComponentInParent<PlayerHealth>();
        if (playerHealth != null)
        {
            playerHealth.TakeDamage(ProjectileDamageStrength);
        }

        Destroy(gameObject);
    }
}
