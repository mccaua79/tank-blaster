﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float Speed;
    public float RotationSpeed;
    private PlayerAttack _weaponController;

    private TankTrackMovement[] tracks;
    private Rigidbody2D _rigidbody2D;
    
    // Use this for initialization
    void Start ()
	{
	    tracks = GetComponentsInChildren<TankTrackMovement>();
	    _rigidbody2D = GetComponent<Rigidbody2D>();
        _weaponController = GetComponentInChildren<PlayerAttack>();
    }

    void Update()
    {
        CheckAttack();
    }
	// Update is called once per frame
	void FixedUpdate ()
	{
	    var rotation = Input.GetAxisRaw("Horizontal");
	    var thrust = Input.GetAxisRaw("Vertical");

	    var trackLength = tracks.Length;
	    var i = 0;
	    for (i = 0; i < trackLength; i++)
	    {
	        var track = tracks[i];
	        if (track.TankTrackName == "Left")
	        {
	            track.Acceleration = Mathf.Abs(thrust);
	        }
            else if (track.TankTrackName == "Right")
            {
                track.Acceleration = Mathf.Abs(thrust);
            }
        }

        CheckMovement(thrust, rotation);
    }

    private void CheckAttack()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            _weaponController.FireWeapon();
        }
    }

    private void CheckMovement(float thrust, float rotation)
    {
        //converting the object euler angle's magnitude from to Radians    
        var angle = (transform.eulerAngles.magnitude + rotation+90f) * Mathf.Deg2Rad;

        //var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        //var rot = Quaternion.LookRotation(Vector3.forward, rotation - transform.position);
        transform.RotateAround(transform.position, transform.forward, Time.deltaTime * -rotation * RotationSpeed);

        var myPos = new Vector2();
        myPos.x = (Mathf.Cos(angle) * thrust * Speed) * Time.deltaTime;
        myPos.y = (Mathf.Sin(angle) * thrust * Speed) * Time.deltaTime;

        //Apply
        _rigidbody2D.velocity = myPos; 
    }
}
