﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallTankController : MonoBehaviour
{
    public Transform SightStart, SightEnd;
    public GameObject WeaponProjectile;
    public GameObject ProjectilePosition;
    public bool Spotted = false, AttackPlayer = false;
    private EnemyHealthController _enemyHealthController;
    private bool CooledDown = true;
    
  //  private GameObject _player;
	// Use this for initialization
	void Start ()
	{
	    _enemyHealthController = GetComponent<EnemyHealthController>();
	    // _player = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update ()
	{
	    CheckLife();
        Raycasting();
        Behaviours();
	}

    void Raycasting()
    {
        Debug.DrawLine(SightStart.position, SightEnd.position, Color.green);

        Spotted = Physics2D.Linecast(SightStart.position, SightEnd.position, 1 << LayerMask.NameToLayer("Player"));
    }

    void Behaviours()
    {
        AttackBehaviour();
    }

    private void AttackBehaviour()
    {
        if (Spotted)
        {
            AttackPlayer = true;
        }

        if (AttackPlayer)
        {
            // TODO: Start locating player

            // Set to attack mode
            if (Spotted)
            {
                // If player is in line of sight. Attack them
                Attack();
            }
        }
    }

    private void Attack()
    {
        if (CooledDown)
        {
            Instantiate(WeaponProjectile, ProjectilePosition.transform.position, ProjectilePosition.transform.rotation);
            CooledDown = false;
            StartCoroutine(ShootProjectile());
        }
    }

    private IEnumerator ShootProjectile()
    {
        yield return new WaitForSeconds(0.25f);
        CooledDown = true;
    }

    void CheckLife()
    {
        if (_enemyHealthController.IsDead())
        {
            // TODO: Build off of this
            Destroy(gameObject);
        }
    }
}
